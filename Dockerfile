FROM selenium/standalone-chrome

WORKDIR /usr/app/online-my-morrisons-automation
#helo
# Install some depenendencies
COPY ./docker-requirements.txt /tmp/docker-requirements.txt
RUN sudo python3.9 -m pip install -r /tmp/docker-requirements.txt
COPY ./ ./
RUN sudo python3.9 -m pip install ./utilities/PyAuto-1.0.0-py2.py3-none-any.whl
RUN export PATH=/usr/app/:$PATH

WORKDIR /usr/app/online-my-morrisons-automation/tests/test_api/

# Default command
ENTRYPOINT ["sudo", "python3.9", "RunTest.py", "--docker"]
CMD ["--tags", "bdd"]

#docker build -t framework/pyauto .
#docker run -it -v %cd%/reports/report_dates/:/usr/app/reports/report_dates/ framework/pyauto --tags se_input_form
